.. _index:
.. admcycles documentation master file


Welcome to admcycles's documentation!
=====================================

Some examples are available in the :ref:`tutorial`

.. toctree::
   :maxdepth: 2

   tutorial
   DRmodule
   admcyclesmodule
   stable_graph
